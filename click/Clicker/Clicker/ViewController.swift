//
//  ViewController.swift
//  Clicker
//
//  Created by Jabuko on 02/06/2020.
//  Copyright © 2020 Jabuko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var butReset: UIButton!
    @IBOutlet var labMoney: UILabel!
    @IBOutlet var butBank: UIButton!
    @IBOutlet var butShop: UIButton!
    @IBOutlet var butSkill: UIButton!
    
    
    
    @IBAction func SkillButton(_ sender: UIButton) {
        useSkill()
        skillCooldown()
    }
    
    @IBAction func BankButton(_ sender: UIButton) {
        let alertController3 = UIAlertController(title:"Bank", message:"Wartosc banku wynosi: " + String(Int(variables.banck)) + ". Czy chcesz wyplacic?" , preferredStyle: .alert)
        let OKAction3 = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            if(variables.banck >= 1.0)
            {
                variables.GibMoniPlox()
            }
            
            self.labMoney.text = variables.giveText()
        }
        let cancelAction3 = UIAlertAction(title: "Cancel", style: .cancel){
            (action:UIAlertAction!) in
        }
        alertController3.addAction(OKAction3)
        alertController3.addAction(cancelAction3)
        self.present(alertController3, animated:true, completion:nil)
        
    }
    @IBAction func ResetButton(_ sender: UIButton) {
        alertReset()
        
    }
    @IBAction func ShopButton(_ sender: UIButton) {
        
        openShop()
        
        
        
    }
    @IBAction func MoneyButton(_ sender: UIButton) {
        
        variables.click()
        labMoney.text = variables.giveText()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named:"background.jpg")
        self.view.insertSubview(backgroundImage, at: 0)
        isEnabled()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        variables()
    }
    

    @objc func appMovedToBackground()
    {
        variables.save()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        variables.save()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        labMoney.text = variables.giveText()
        isEnabled()
    }


    func alertReset()
    {
        let alertController = UIAlertController(title:"Reset", message:"Czy na pewno chcesz zresetowac swoj postep?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            if (variables.money >= 10000 && variables.resetLvl==0)
            {
                variables.money = 0
                variables.banck = 0
                variables.rlMoney = 0
                variables.rlMul = 1
                variables.item1 = 10
                variables.resetLvl = 1
                variables.multCost = 10
                variables.multRlCost = 10
                variables.value = Double(variables.baseValue * (variables.item1 + variables.item2 + variables.item3)) * variables.rlMul
                self.labMoney.text = "0"
                let alertControllerItem1 = UIAlertController(title:"Brawo!", message:"Otrzymales Rozdzke Porannego Oddechu! Twoj mnoznik zwiekszyl sie!", preferredStyle: .alert)
                let dismissActionItem1 = UIAlertAction(title: "Dzieki!", style: .cancel){
                    (action:UIAlertAction!) in
                    
                }
                alertControllerItem1.addAction(dismissActionItem1)
                self.present(alertControllerItem1, animated:true, completion:nil)
            }
            else if(variables.money >= 100000 && variables.resetLvl == 1)
            {
                variables.money = 0
                variables.banck = 0
                variables.rlMoney = 0
                variables.rlMul = 1
                variables.item2 = 20
                variables.multCost = 10
                variables.multRlCost = 10
                variables.resetLvl = 2
                variables.value = Double(variables.baseValue * (variables.item1 + variables.item2 + variables.item3)) * variables.rlMul
                self.labMoney.text = "0"
                let alertControllerItem2 = UIAlertController(title:"Brawo!", message:"Otrzymales Tarcze Pepkowego Paprocha! Twoj mnoznik zwiekszyl sie!", preferredStyle: .alert)
                let dismissActionItem2 = UIAlertAction(title: "Dzieki!", style: .cancel){
                    (action:UIAlertAction!) in
                    
                }
                alertControllerItem2.addAction(dismissActionItem2)
                self.present(alertControllerItem2, animated:true, completion:nil)
            }
            else if(variables.money >= 1000000 && variables.resetLvl == 2)
            {
                variables.money = 0
                variables.banck = 0
                variables.rlMoney = 0
                variables.rlMul = 1
                variables.multCost = 10
                variables.multRlCost = 10
                variables.item3 = 30
                variables.resetLvl = 3
                variables.value = Double(variables.baseValue * (variables.item1 + variables.item2 + variables.item3)) * variables.rlMul
                self.labMoney.text = "0"
                let alertControllerItem3 = UIAlertController(title:"Brawo!", message:"Otrzymales Lustro Zielonej Kozy! Twoj mnoznik zwiekszyl sie!", preferredStyle: .alert)
                let dismissActionItem3 = UIAlertAction(title: "Dzieki!", style: .cancel){
                    (action:UIAlertAction!) in
                    
                }
                alertControllerItem3.addAction(dismissActionItem3)
                self.present(alertControllerItem3, animated:true, completion:nil)
            }
            else if(variables.money >= 10000000 && variables.resetLvl >= 3)
            {
                variables.money = 0
                variables.banck = 0
                variables.rlMoney = 0
                variables.multCost = 10
                variables.multRlCost = 10
                variables.rlMul = 1
                variables.resetLvl += 1
                variables.value = Double(variables.baseValue * (variables.item1 + variables.item2 + variables.item3)) * variables.rlMul
                self.labMoney.text = "0"
            }
            else
            {
                var shortCash = 0
                if(variables.resetLvl == 0)
                {
                    shortCash = 10000 - variables.money
                }
                else if(variables.resetLvl == 1)
                {
                    shortCash = 100000 - variables.money
                }
                else if(variables.resetLvl == 2)
                {
                    shortCash = 1000000 - variables.money
                }
                else
                {
                    shortCash = 10000000 - variables.money
                }
                let alertController2 = UIAlertController(title:"Uwaga!", message:"Nie masz wystarczajaco pieniedzy. Brakuje ci: " + String(shortCash), preferredStyle: .alert)
                let dismissAction = UIAlertAction(title: "Okej, ide zarabiac dalej!", style: .cancel){
                    (action:UIAlertAction!) in
                    
                }
                alertController2.addAction(dismissAction)
                self.present(alertController2, animated:true, completion:nil)
            }
            
        }
        
        alertController.addAction(OKAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){
            (action:UIAlertAction!) in
            
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated:true, completion:nil)
        
    }
    
    func skillCooldown()
    {
        var countdown = 20
        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats:true)
        {
            timer in
            self.butSkill.setTitle(String(countdown), for: .normal)
            variables.isSkillOnCooldown = true
            
            countdown-=1
        if(countdown == -1)
        {
            self.butSkill.isEnabled = true
            self.butSkill.setTitle("", for: .normal)
            timer.invalidate()
            variables.isSkillOnCooldown = false
            }
            
        }
            
          
        
    }
    func useSkill(){
        butSkill.isEnabled = false
        butShop.isEnabled = false
        butReset.isEnabled = false
        var count = 0
        let timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats:true)
        {
            timer in
            variables.click()
            self.labMoney.text = variables.giveText()

            
            count+=1
            if(count == variables.skillDuration)
        {
            self.butShop.isEnabled = true
            self.butReset.isEnabled = true
            timer.invalidate()
            }
            
        }
    }
    
    func openShop()
    {
        let storyboard = UIStoryboard(name: "Main", bundle:nil)
        let secondVC = storyboard.instantiateViewController(withIdentifier: "ShopViewController")as!ShopViewController
        self.navigationController?.pushViewController(secondVC, animated:true)
    }
    
    func isEnabled()
    {
        if(variables.bankIsEnabled)
        {
            butBank.isEnabled = true
        }
        else
        {
            butBank.isEnabled = false
        }
        
        if(variables.skillIsEnabled && variables.isSkillOnCooldown == false)
        {
            butSkill.isEnabled = true
        }
        else
        {
            butSkill.isEnabled = false
        }
    }
    
}

class variables{
    public static var isAuthorized = false
    public static var baseValue = 1
    public static var banck = 0.0
    public static var item1 = 1
    public static var item2 = 0
    public static var item3 = 0
    public static var resetLvl = 0
    public static var money = 0
    public static var value = 1.0
    public static var rlMul = 1.0
    public static var rlMoney = 500000.0
    public static var skillDuration = 0
    public static var bankIsEnabled = false
    public static var skillIsEnabled = false
    public static var isSkillOnCooldown = false
    public static var isSkillEnabledShop = true
    public static var isBankEnabledShop = true
    public static var multCost = 10
    public static var multRlCost = 10.0
    public static var defaults = UserDefaults.standard
    
    
    static func click(){
        rlMoney = rlMoney+value
        money = Int(rlMoney)
        banck += Double((rlMoney/10000.0)*value)
        
    }
    static func giveText()->String{
        return String(money)
    }
    
    static func GibMoniPlox()
    {
        money += Int(banck)
        rlMoney += Double(Int(banck))
        banck = 0.0
    }
    
    init()
    {
        if(variables.isAuthorized)
        {
            variables.money = variables.defaults.integer(forKey: "money")
            variables.rlMoney = variables.defaults.double(forKey: "rlMoney")
            variables.baseValue = variables.defaults.integer(forKey: "baseValue")
            variables.banck = variables.defaults.double(forKey: "banck")
            variables.item1 = variables.defaults.integer(forKey: "item1")
            variables.item2 = variables.defaults.integer(forKey: "item2")
            variables.item3 = variables.defaults.integer(forKey: "item3")
            variables.resetLvl = variables.defaults.integer(forKey: "resetLvl")
            variables.value = variables.defaults.double(forKey: "value")
            variables.rlMul = variables.defaults.double(forKey: "rlMul")
            variables.skillDuration = variables.defaults.integer(forKey: "skillDuration")
            variables.bankIsEnabled = variables.defaults.bool(forKey: "bankIsEnabled")
            variables.skillIsEnabled = variables.defaults.bool(forKey: "skillIsEnabled")
            variables.isSkillOnCooldown = variables.defaults.bool(forKey: "isSkillOnCooldown")
            variables.isSkillEnabledShop = variables.defaults.bool(forKey: "isSkillEnabledShop")
            variables.isBankEnabledShop = variables.defaults.bool(forKey: "isBankEnabledShop")
            variables.multCost = variables.defaults.integer(forKey: "multCost")
            variables.multRlCost = variables.defaults.double(forKey: "multRlCost")
        }
            
    }
    
 
    static func save()
    {
        variables.defaults.set(variables.money, forKey: "money")
        variables.defaults.set(variables.rlMoney, forKey: "rlMoney")
        variables.defaults.set(variables.baseValue, forKey: "baseValue")
        variables.defaults.set(variables.banck, forKey: "banck")
        variables.defaults.set(variables.item1, forKey: "item1")
        variables.defaults.set(variables.item2, forKey: "item2")
        variables.defaults.set(variables.item3, forKey: "item3")
        variables.defaults.set(variables.resetLvl, forKey: "resetLvl")
        variables.defaults.set(variables.value, forKey: "value")
        variables.defaults.set(variables.rlMul, forKey: "rlMul")
        variables.defaults.set(variables.skillDuration, forKey: "skillDuration")
        variables.defaults.set(variables.bankIsEnabled, forKey: "bankIsEnabled")
        variables.defaults.set(variables.skillIsEnabled, forKey: "skillIsEnabled")
        variables.defaults.set(variables.isSkillOnCooldown, forKey: "isSkillOnCooldown")
        variables.defaults.set(variables.isSkillEnabledShop, forKey: "isSkillEnabledShop")
        variables.defaults.set(variables.isBankEnabledShop, forKey: "isBankEnabledShop")
        variables.defaults.set(variables.multCost, forKey: "multCost")
        variables.defaults.set(variables.multRlCost, forKey: "multRlCost")
    }
    
}

