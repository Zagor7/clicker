import UIKit

class ShopViewController: UIViewController {
    
    @IBOutlet var buyBankBut: UIButton!
    @IBOutlet var butSkillBut: UIButton!
    @IBOutlet var costMult: UILabel!
    @IBOutlet var valueMult: UILabel!
    @IBAction func increaseMult(_ sender: UIButton) {
        buyIncreaser()
    }
    @IBAction func buyBank(_ sender: UIButton) {
        buyBankFunc()
        
    }
    @IBAction func buySkill(_ sender: UIButton) {
        buySkillFunc()
        
    }
    
    
    
    override func viewDidLoad() {
        costMult.text = "Koszt: " + String(variables.multCost)
        valueMult.text = "Mnoznik: " + String(format: "%.1f", variables.rlMul)
        super.viewDidLoad()
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named:"tlo.png")
        self.view.insertSubview(backgroundImage, at: 0)
        isEnabled()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        costMult.text = "Koszt: " + String(variables.multCost)
        valueMult.text = "Mnoznik: " + String(format: "%.1f", variables.rlMul)
        isEnabled()
    }
    
    func isEnabled()
    {
        if(variables.isBankEnabledShop)
        {
            buyBankBut.isEnabled = true
        }
        else
        {
            buyBankBut.isEnabled = false
        }
        
        if(variables.isSkillEnabledShop)
        {
            butSkillBut.isEnabled = true
        }
        else
        {
            butSkillBut.isEnabled = false
        }
        
    }
    
    @IBAction func backTest(_ sender: UIButton) {
        
        navigationController?.popViewController(animated: true)
        
    }
    
    func buyBankFunc()
    {
        let alertController3 = UIAlertController(title:"Bank", message:"Czy chcesz kupic skill bank za 5000?", preferredStyle: .alert)
               let OKAction3 = UIAlertAction(title: "OK", style: .default){
                   (action:UIAlertAction!) in
                if(variables.money < 5000)
                {
                    let shortCash = 5000 - variables.money
                    let alertController2 = UIAlertController(title:"Uwaga!", message:"Nie masz wystarczajaco pieniedzy. Brakuje ci: " + String(shortCash), preferredStyle: .alert)
                    let dismissAction = UIAlertAction(title: "Okej, ide zarabiac dalej!", style: .cancel){
                        (action:UIAlertAction!) in
                        
                    }
                    alertController2.addAction(dismissAction)
                    self.present(alertController2, animated:true, completion:nil)
                }
                else
                {
                    variables.banck = 0.0
                    variables.rlMoney -= 5000
                    variables.money = Int(variables.rlMoney)
                    variables.bankIsEnabled = true
                    let alertController2 = UIAlertController(title:"Gratulacje!", message: "Kupiles bank!", preferredStyle: .alert)
                    let dismissAction = UIAlertAction(title: "Dzieki!", style: .cancel){
                        (action:UIAlertAction!) in
                        
                    }
                    alertController2.addAction(dismissAction)
                    self.present(alertController2, animated:true, completion:nil)
                    variables.isBankEnabledShop = false
                    self.isEnabled()
                                    }
                   
               }
               let cancelAction3 = UIAlertAction(title: "Cancel", style: .cancel){
                   (action:UIAlertAction!) in
               }
               alertController3.addAction(OKAction3)
               alertController3.addAction(cancelAction3)
               self.present(alertController3, animated:true, completion:nil)
        
    }
    

    func buySkillFunc()
    {
        if(variables.skillDuration == 0)
        {
            let alertController3 = UIAlertController(title:"Skill", message:"Czy chcesz kupic 1 poziom skilla za 10000?", preferredStyle: .alert)
            let OKAction3 = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
             if(variables.money >= 10000)
             {
                variables.skillIsEnabled = true
                variables.skillDuration = 50
                variables.rlMoney -= 10000
                variables.money = Int(variables.rlMoney)
                let alertController2 = UIAlertController(title:"Gratulacje!", message: "Kupiles pierwszy poziom skilla!", preferredStyle: .alert)
                let dismissAction = UIAlertAction(title: "Dzieki!", style: .cancel){
                    (action:UIAlertAction!) in
                    
                }
                alertController2.addAction(dismissAction)
                self.present(alertController2, animated:true, completion:nil)
                
             }
             else
             {
                 let shortCash = 10000 - variables.money
                 let alertController2 = UIAlertController(title:"Uwaga!", message:"Nie masz wystarczajaco pieniedzy. Brakuje ci: " + String(shortCash), preferredStyle: .alert)
                 let dismissAction = UIAlertAction(title: "Okej, ide zarabiac dalej!", style: .cancel){
                     (action:UIAlertAction!) in
                     
                 }
                 alertController2.addAction(dismissAction)
                 self.present(alertController2, animated:true, completion:nil)
                                 }
                
            }
            let cancelAction3 = UIAlertAction(title: "Cancel", style: .cancel){
                (action:UIAlertAction!) in
            }
            alertController3.addAction(OKAction3)
            alertController3.addAction(cancelAction3)
            self.present(alertController3, animated:true, completion:nil)
        }
        else if(variables.skillDuration == 50)
        {
            let alertController3 = UIAlertController(title:"Skill", message:"Czy chcesz kupic 2 poziom skilla za 50000?", preferredStyle: .alert)
            let OKAction3 = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
             if(variables.money >= 50000)
             {
                variables.skillDuration = 100
                variables.rlMoney -= 50000
                variables.money = Int(variables.rlMoney)
                let alertController2 = UIAlertController(title:"Gratulacje!", message: "Kupiles drugi poziom skilla!", preferredStyle: .alert)
                let dismissAction = UIAlertAction(title: "Dzieki!", style: .cancel){
                    (action:UIAlertAction!) in
                    
                }
                alertController2.addAction(dismissAction)
                self.present(alertController2, animated:true, completion:nil)
                
             }
             else
             {
                 let shortCash = 50000 - variables.money
                 let alertController2 = UIAlertController(title:"Uwaga!", message:"Nie masz wystarczajaco pieniedzy. Brakuje ci: " + String(shortCash), preferredStyle: .alert)
                 let dismissAction = UIAlertAction(title: "Okej, ide zarabiac dalej!", style: .cancel){
                     (action:UIAlertAction!) in
                     
                 }
                 alertController2.addAction(dismissAction)
                 self.present(alertController2, animated:true, completion:nil)
                                 }
                
            }
            let cancelAction3 = UIAlertAction(title: "Cancel", style: .cancel){
                (action:UIAlertAction!) in
            }
            alertController3.addAction(OKAction3)
            alertController3.addAction(cancelAction3)
            self.present(alertController3, animated:true, completion:nil)
        }
        else if (variables.skillDuration == 100)
        {
            let alertController3 = UIAlertController(title:"Skill", message:"Czy chcesz kupic 3 poziom skilla za 100000?", preferredStyle: .alert)
            let OKAction3 = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
             if(variables.money >= 100000)
             {
                variables.skillDuration = 150
                variables.rlMoney -= 100000
                variables.money = Int(variables.rlMoney)
                let alertController2 = UIAlertController(title:"Gratulacje!", message: "Kupiles trzeci poziom skilla!", preferredStyle: .alert)
                let dismissAction = UIAlertAction(title: "Dzieki!", style: .cancel){
                    (action:UIAlertAction!) in
                    
                }
                alertController2.addAction(dismissAction)
                self.present(alertController2, animated:true, completion:nil)
                variables.isSkillEnabledShop = false
                self.isEnabled()
                
             }
             else
             {
                 let shortCash = 100000 - variables.money
                 let alertController2 = UIAlertController(title:"Uwaga!", message:"Nie masz wystarczajaco pieniedzy. Brakuje ci: " + String(shortCash), preferredStyle: .alert)
                 let dismissAction = UIAlertAction(title: "Okej, ide zarabiac dalej!", style: .cancel){
                     (action:UIAlertAction!) in
                     
                 }
                 alertController2.addAction(dismissAction)
                 self.present(alertController2, animated:true, completion:nil)
                                 }
                
            }
            let cancelAction3 = UIAlertAction(title: "Cancel", style: .cancel){
                (action:UIAlertAction!) in
            }
            alertController3.addAction(OKAction3)
            alertController3.addAction(cancelAction3)
            self.present(alertController3, animated:true, completion:nil)
        }
    }
    func buyIncreaser()
    {
        if(variables.multRlCost <= variables.rlMoney)
        {
            variables.rlMoney -= variables.multRlCost
            variables.money = Int(variables.rlMoney)
            variables.rlMul += 0.1
            variables.value = Double(variables.baseValue * (variables.item1 + variables.item2 + variables.item3)) * variables.rlMul
            variables.multRlCost += variables.multRlCost*variables.rlMul/2
            variables.multCost = Int(variables.multRlCost)
            costMult.text = "Koszt: " + String(variables.multCost)
            valueMult.text = "Mnoznik: " + String(format: "%.1f", variables.rlMul)
        }
        else
        {
            let alertController2 = UIAlertController(title:"Uwaga!", message:"Nie masz wystarczajaco pieniedzy.", preferredStyle: .alert)
            let dismissAction = UIAlertAction(title: "Okej, ide zarabiac dalej!", style: .cancel){
                (action:UIAlertAction!) in
                
            }
            alertController2.addAction(dismissAction)
            self.present(alertController2, animated:true, completion:nil)
            
        }
    }
}
